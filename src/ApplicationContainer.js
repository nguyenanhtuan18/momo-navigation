import React from 'react'
import ApplicationTab from './ApplicationTab'
import Application from './Application'

const APPLICATION_TAB = 'ApplicationTab';

export default function ApplicationContainer(props) {

    const { route: { params: {feature = {}} = {} } } = props
    const { config: { mainApp = APPLICATION_TAB } = {} } = feature

    if (mainApp === APPLICATION_TAB) {
        return (<ApplicationTab {...props} />)
    }

    return <Application {...props} />

}