import React from 'react'
import {View, Text} from 'react-native'
import MiniApp from './app-mini/MiniApp'

export default function Application(props){
    console.log('Application.props', props);
    return (
        <View style={{flex: 1}}>
            <MiniApp {...props}/>
        </View>
    )
}