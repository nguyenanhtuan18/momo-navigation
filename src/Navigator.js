
//momo-app/src/app-core/navigator/Navigation.js
class Navigator {

    setRootNavigator(navigationRef = {}) {
        if (!this.navigator || navigationRef) {
            this.navigator = navigationRef;
        }
    }

    //momo-app/src/app-core/navigator/PlatformNavigationStrategy
    startFeature = (screen) => {
        if (this.navigator) {
            console.log(this.navigator.getRootState());
            this.navigator.navigate('RootStack', {
                screen: 'Application',
                params: { feature: { screen, config: { mainApp: 'miniApp' } } },
            });
        }
    }

    dismiss = (navigation) => {
        if(this.navigator){
            console.log(this.navigator)
            console.log('getCurrentRoute', this.navigator.getCurrentRoute())
            console.log('canGoBack', this.navigator.canGoBack())
            console.log('getRootState', this.navigator.getRootState())
            this.navigator.goBack()
        } 
    }
}

export default new Navigator();