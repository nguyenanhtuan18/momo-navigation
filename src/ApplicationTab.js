import React from 'react'
import { View, Text } from 'react-native'
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs'
import Application from './Application';

export const FEATURES = [
    {
        name: 'Momo',
        code: 'momo'
    },
    {
        name: 'Ưu đãi',
        code: 'uu_dai'
    },
    {
        name: 'Lịch sử GD',
        code: 'lich_su_gd'
    },
    {
        name: 'Chat',
        code: 'chat'
    },
    {
        name: 'Ví của tôi',
        code: 'vi_cua_toi'
    }
]

const Tab = createBottomTabNavigator();

export default function ApplicationTab() {
    const TabScreens = FEATURES.map((feature, index) => {
        const { name, code } = feature
        return (
            <Tab.Screen
                key={code}
                name={name}
                component={Application}
                initialParams={{ index, feature }}
            />
        )
    })

    return (
        <Tab.Navigator
            backBehavior={'none'}
            tabBarOptions={{
                backBehavior: 'none',
                keyboardHidesTabBar: Platform.OS === 'android'
            }}>
            {TabScreens}
        </Tab.Navigator>
    )
}