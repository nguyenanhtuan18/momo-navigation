/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */
import 'react-native-gesture-handler';
import React from 'react';
import { View } from 'react-native';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator, TransitionPresets } from '@react-navigation/stack';
import ApplicationContainer from './ApplicationContainer'
import Navigator from './Navigator';

const ModalStack = createStackNavigator();
const RootStack = createStackNavigator();

const ScreenStack = () => {
  return (
    <ModalStack.Navigator headerMode="none" initialRouteName={'Application'} keyboardHandlingEnabled={false}>
      <ModalStack.Screen
        name="Application"
        component={ApplicationContainer}
        options={{
          cardStyle: { backgroundColor: 'transparent' },
          cardOverlayEnabled: false,
        }}
      />
    </ModalStack.Navigator>
  )
}

const App = () => {
  return (
    <View style={{ flex: 1 }}>
      <NavigationContainer 
        ref={navigationRef => {
          Navigator.setRootNavigator(navigationRef);
        }}
      >
        <RootStack.Navigator headerMode="none" initialRouteName="Screen" keyboardHandlingEnabled={false}>
          <RootStack.Screen
            name="RootStack"
            component={ScreenStack}
            options={{
              gestureEnabled: true,
              cardStyle: {
                backgroundColor: 'transparent',
                cardOverlayEnabled: false,
              },
              ...TransitionPresets.SlideFromRightIOS,
            }}
          />
        </RootStack.Navigator>
      </NavigationContainer>
    </View>
  )
};

export default App;
