var { SHA256 } = require('crypto-js');

export default {
    sha256(message) {
        return SHA256(message).toString();
    }
}