import React, {useRef, useImperativeHandle } from 'react';
import { createStackNavigator } from '@react-navigation/stack';
import { NavigationContainer } from '@react-navigation/native';
import { Animated } from 'react-native';
import StackScreen from './StackScreen'

const Stack = createStackNavigator();
const Modal = createStackNavigator();

const forAnimationHeader = ({ current, next }) => {
    const progress = Animated.add(
        current.progress.interpolate({
            inputRange: [0, 1],
            outputRange: [0, 1],
            extrapolate: 'clamp',
        }),
        next
            ? next.progress.interpolate({
                inputRange: [0, 1],
                outputRange: [0, 1],
                extrapolate: 'clamp',
            })
            : 0,
    );

    const opacity = progress.interpolate({
        inputRange: [0, 1, 2],
        outputRange: [0, 1, 0],
        extrapolate: 'clamp',
    });

    return {
        leftButtonStyle: { opacity },
        rightButtonStyle: { opacity },
        titleStyle: { opacity },
        backgroundStyle: { opacity },
    };
};

const StackNavigator = ({ route }) => {
    const { params } = route;
    return (
        <Stack.Navigator
            screenOptions={{
                ...params.defaultOption,
                headerStyleInterpolator: forAnimationHeader,
                gestureEnabled: true,
                gestureDirection: 'horizontal',
            }}>
            <Stack.Screen
                name="StackScreen"
                component={StackScreen}
                initialParams={params}
            />
        </Stack.Navigator>
    )
}

const Navigation = (props, ref) => {
    const navigatorRef = useRef(null);
    useImperativeHandle(ref, () => navigatorRef.current);
    return (
        <NavigationContainer ref={navigatorRef} independent>
            <Modal.Navigator initialRouteName="Stack" headerMode="screen">
                <Modal.Screen
                    name="Stack"
                    component={StackNavigator}
                    initialParams={props}
                    options={{
                        headerShown: false,
                        cardStyle: { backgroundColor: 'transparent' }
                    }}
                />
            </Modal.Navigator>
        </NavigationContainer>
    )
}

export default React.forwardRef(Navigation);