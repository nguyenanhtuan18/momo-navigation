import React from 'react'
import { View, Text, Button } from 'react-native'
import Navigation from './Navigation'
import Navigator from '../Navigator'

const ListRoomScreen = ({ navigation }) => {
    return (
        <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
            <Text>List Room Screen</Text>
            <Button title={'Go to ChatConversation'} onPress={() => {
                //navigation.navigate('ChatConversationScreen')
                console.log('ListRoomScreen.navigation', navigation);
            }} />

            <Button title={'Start Feature ChatConversation'} onPress={() => {
                Navigator.startFeature('ChatConversationScreen')
            }} />
        </View>
    )
}

const ChatConversationScreen = ({ navigation }) => {
    return (
        <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
            <Text>Chat Conversation Screen</Text>
            <Button title={'Back'} onPress={() => {
                Navigator.dismiss(navigation)
            }} />
        </View>
    )
}

const SCREENS = {
    ListRoomScreen,
    ChatConversationScreen
}

export default function MiniApp(props) {
    const { route: {params: {feature : {screen = 'ListRoomScreen'}}} } = props;

    const App = SCREENS[screen] || ListRoomScreen

    return (
        <Navigation
            screen={screenProps => {
                console.log('MiniApp.Navigation.screenProps', screenProps);
                return (<App {...screenProps}/>)
            }}
        />
    )
}